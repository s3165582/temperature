package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private Map<String, Double> prices = new HashMap<>();
    public double getBookPrice(String isbn){
        prices.put("1",10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
        if (prices.containsKey(isbn)){
            return prices.get(isbn);
        }else {
            return 0;
        }
    }
}
